### Intro to Machine Learning Problem Framing

## Overview
#### Common ML Problems
* Supervised vs. Unsupervised
* More accurate to think in terms of a spectrum of supervision (supervised <-> unsupervised)
* Supervised - labeled training data
    * during training, algorithm determines relationship (i.e. **model**) b/w features and labels
* Unsupervised - unlabeled, goal is to identify meaningful pattens

#### Reinforcement Learning
* additional branch of ML
* an agent (model) attempts to maximize the amount of reward it receives while interacting with an environment
    * Teaching a computer to play video games (goal: don't die)

![](types-ml-problems.png)

## Getting Started with ML
#### The ML Mindset
>"Machine Learning changes the way you think about a problem. The focus shifts from a mathematical science to a natural science, running experiments and using statistics, not logic, to analyze its results." - Peter Norvig - Google Research Director

* Helpful to think of ML process as running an experiment over and over again until a workable model is created
* Scientific method:
    1. Set research goal
        * Traffic congestion on a given day
    2. Make a hypothesis
        * Weather influences congestion
    3. Collect the data
    4. Test your hypothesis
    5. Analyze your results
        * is model better than existing solutions
    6. Reach a conclusion
    7. Refine hypothesis and repeat

#### Identifying Good Problems for ML
* Focus on problems that are difficult to solve using traditional programming techniques
* Ask yourself:
    1. What problem is my product facing?
    2. Would it be a good problem for ML?
* Data volume is important
    * 1,000+ examples for basic linear models
    * 100,000+ examples for neural networks
* Predictive power
    * don't rely on ML to discover which features are relevant; can create complex models
* ```ML is better at making decisions than giving insights```
* If goals is to find out "interesting" things, statistical approaches make more sense
* Ensure predictions created by ML allow for a useful action (e.g. YouTube and next videos)

![](predictions-vs-decisions.png)

#### Hard ML Problems
* Clustering
* Anomaly detection
* Causation
* No existing data

## Framing a Problem
#### Deciding on ML
* Use plain terms to describe what would you like your model to do
    * Ex: We want the ML model to predict how popular a video just uploaded will become in the future
* What is the ideal outcome?
    * Ex: Our ideal outcome is to suggest videos that people find worth their time
* Success & Failure Metrics
    * beyond precision, recall, AUC
    * Success: reducing CPU cost by X%; Failure: cost reduction < cost to train and serve the model
#### What type of output are you looking for?
* Good output - quantifiable with a clear definition that a machine can produce
* How will you use the predictions?
    * if probability of event X > 0.90, send notification

#### Heuristics
* Never launch a ML model that can't beat a heuristic
* Making a heuristic can help identify good signals in your ML model
* Non-ML solutions can have advantage of being simpler to maintain than ML solutions

#### Framing Recap
1. Start Clearly and Simply
2. Your Ideal Outcome (independent of the model)
3. Your Success Metrics (independent of model evaluation metrics)
    * "Our success metric are..."
    * "Our key results for the success metrics are..."
    * "Our model is deemed a failure if..."
4. Your Output
    * "The output from our model will be..."
    * "The output is defined as..."
5. Using the Output
    * "The output from the ML model will be made..."
    * "The outcome will be used for..."
6. Your Heuristics
    * "If we didn't use ML, we would..."

#### Formulating a Problem
1. Articulate the Problem
    * Explain what the model will predict
    > "Our problem is best framed as a 4-class, single label classification which predicts whether a payor belongs to one of 4 classes: private, medicare, medicaid, other"

2. Start simple
    * Try using simplest model possible (easier to implement and understand)
    * **Simple model = baseline**
    * Build pipeline before getting crazy with it
3. See if labeled data exists
4. Design your data for the model
5. Determine where the data comes from
6. Determine easily obtained inputs
    * Pick **1-3 inputs** that easily obtained and that you think would produce a reasonable, initial outcome

#### Key Takeaways
* You want your ML model to make **decisions** - not just predictions
* ML problem is well-defined if you have **identified inputs and outputs**
* When starting a project, **pick 1-3 features** that seem to have strong predictive power
* **Do not search for correlations** before defining your ML problem

## Conclusion
* Keep learning:
https://developers.google.com/machine-learning/crash-course/
